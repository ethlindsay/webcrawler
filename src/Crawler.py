from urllib.parse import urlparse
import requests
from bs4 import BeautifulSoup
import sys

def crawl(url, depth, maxdepth, visited):
    if depth == maxdepth:
        return
    currentUrlHtml = getSiteInfo(url)
    soup = BeautifulSoup(currentUrlHtml, features="lxml")
    hyperlinks = soup.find_all('a')
    for i in hyperlinks:
        # uses string operations to isolate the url portion of the HTML code
        str(i).replace("a href=\"","")
        j = str(i)[str(i).find("\"") + 1:str(i).find("\"", str(i).find("\"") +1)]
        if j.startswith("/"):
            j = url + j
        t = getSiteInfo(j)
        if i != "":
            parsed = urlparse(j)
            if parsed[0] == "":
                j = urlparse(url)[0] + "://" + j
        if (j in visited) == False:
            if t != "":
                visited.append(j)
                print(("    " * depth) + j)
        if t != "":
            crawl(j, depth + 1, maxdepth, visited)

def getSiteInfo(url):
    try:
        html = requests.get(url).text
        return html
    except:
        return ""

def main():
    maxDepth = 3
    url = sys.argv[1]
    if len(sys.argv) == 3:
        maxDepth = sys.argv[2]
    try:
        requests.get(url)
    except:
        print("Error: Invalid URL supplied")
        print("Please supply an absolute URL to this program")
    crawl(sys.argv[1],0,maxDepth,[])

main()
